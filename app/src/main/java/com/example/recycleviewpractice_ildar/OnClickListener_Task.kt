package com.example.recycleviewpractice_ildar

import android.view.View
import com.example.recycleviewpractice_ildar.models.Task

interface OnClickListener_Task {

//    fun onLongClick(task: Task, view: View)
    fun onClick(task: Task, view: View)

}
