package com.example.recycleviewpractice_ildar.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.recycleviewpractice_ildar.API.InterfaceApi
import com.example.recycleviewpractice_ildar.API.InterfaceApi.Companion.createRetrofit
import com.example.recycleviewpractice_ildar.models.Lista
import com.example.recycleviewpractice_ildar.models.Task
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.get
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ViewModel: ViewModel() {


    val listaModel= MutableLiveData<MutableList<Lista>>()
    val taskModel= MutableLiveData<MutableList<Task>>()

    var listas = mutableListOf<Lista>() //todos las listas
    var taskas = mutableListOf<Task>() //las tareas de cada lista
    //var todasTaskas= mutableListOf<Task>() //todas las tareas


    //POST METHODS

    fun createNewList(name:String){
        val service=createRetrofit()
        val requestBody=rawJSONList(name)

        //listas.forEach {lista -> println(lista.toString()) }

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = service.postLists("/todolists",requestBody)  //creamos nueva lista en la api

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    debugMessage(requestBody,1, "CREATE LIST")
                    getLists() //cuando el thread termina actualizamos las listas

                } else {

                    Log.e("CREATE RETROFIT_ERROR", response.code().toString())

                }
            }
        }

    }

    fun createNewTask(list:Lista, content:String){ //creamos nueva tarea en una lista
        val service=createRetrofit()
        val requestBody=rawJSONTask(content)

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = service.postItem("/todolists/${list.id}/todoitems",requestBody)

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    debugMessage(requestBody,1, "CREATE TASK ")
                    getTasks(list)

                } else {

                    Log.e("CREATE RETROFIT_ERROR", response.code().toString())

                }
            }
        }
        

    }


    //PUT METHODS

    fun setTaskAtributes(task:Task, list: Lista){

        val service=createRetrofit()
        val requestBody=rawJSONTaskPut(task);

        CoroutineScope(Dispatchers.IO).launch {

            // Do the PUT request and get response
            val response = service.updateTask("/todolists/${list.id}/todoitems",requestBody)

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    debugMessage(requestBody,1, " SET TASK")
                    getTasks(list)

                } else {

                    Log.e("SET RETROFIT_ERROR", response.code().toString())

                }
            }
        }
    }

    fun setListAtributes(list: Lista){ //CAMBIA METODO EN LA API PARA QUE SEA /todolists/id

        val service=createRetrofit()
        val requestBody=rawJSONListPut(list);

        CoroutineScope(Dispatchers.IO).launch {

            // Do the PUT request and get response
            val response = service.updateList("/todolists",requestBody)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    debugMessage(requestBody,1, "SET LIST ")
                    getLists()

                } else {

                    Log.e("RETROFIT_ERROR", response.code().toString())

                }
            }
        }
    }


    //DELETE METHODS

    fun deleteTask(task:Task, list: Lista) {
        val service=createRetrofit()

        CoroutineScope(Dispatchers.IO).launch {
            // Do the DELETE request and get response
            val response = service.deleteTask("/todolists/${list.id}/todoitems/${task.id}")
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    debugMessage(null,2," DELETE TASK ")
                    getTasks(list)


                } else {

                    Log.e("RETROFIT_ERROR", response.code().toString())

                }
            }
        }

    }

    fun deleteList( list: Lista) {

      val service=createRetrofit()

        CoroutineScope(Dispatchers.IO).launch {
            // Do the DELETE request and get response
            val response = service.deleteList("/todolists/${list.id}")
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    debugMessage(null,2, " DELETE LIST ")
                    getLists()

                } else {

                    Log.e("RETROFIT_ERROR", response.code().toString())

                }
            }
        }
    }


    //GET METHODS

    fun getLists(){
        println("\n ENTRAMOS EN GET LISTAS \n")
        val service=createRetrofit()

        CoroutineScope(Dispatchers.IO).launch {
            // Do the DELETE request and get response
            val response = service.getLists("/todolists/")
            val listArray=response.body()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    // Convert raw JSON to pretty JSON using GSON library
                    val gson = GsonBuilder().setPrettyPrinting().create()
                    Log.d("Pretty Printed JSON :", gson.toString())

                    if (listArray != null) {
                        listas=listArray
                        listas.forEach{lista -> println(lista.toString()) }
                        listaModel.postValue(listas)
                        println("GET LIST FUNCIONA")
                    }


                } else {

                    Log.e("RETROFIT_ERROR", response.code().toString())

                }
            }
        }

    }

    fun getTasks(list:Lista){

        println("\n ENTRAMOS EN GET ITEMS \n")
        //vaciamos las taskas
        var emptyTasks= mutableListOf<Task>()
        taskas=emptyTasks //vaciamos taskas

        val service=createRetrofit()

        CoroutineScope(Dispatchers.IO).launch {
            // Do the DELETE request and get response
            val response = service.getTasks("/todolists/${list.id}/todoitems")
            val taskArray=response.body()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {


                    if (taskArray != null) {
                        taskas=taskArray
                        taskModel.postValue(taskas)

                    }

                } else {

                    Log.e("RETROFIT_ERROR", response.code().toString())

                }
            }
        }

    }


    //MISC METHODS

    fun debugMessage(rb:RequestBody?, type:Int,process:String){

        if(type==1){
            println(process+"  exitoso  request body "+rb.toString())

        }else{
            println(process+" exitoso")
        }

    }

    fun rawJSONList(name: String):RequestBody {
        // Create JSON using JSONObject
        val jsonObject = JSONObject()
        jsonObject.put("nomLlista", name)

        // Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        return requestBody
    }

    fun rawJSONListPut(list: Lista):RequestBody {
        // Create JSON using JSONObject
        val jsonObject = JSONObject()
        jsonObject.put("id", list.id)
        jsonObject.put("nomLlista", list.name)

        // Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        return requestBody
    }

    fun rawJSONTask(content: String):RequestBody {
        // Create JSON using JSONObject
        val jsonObject = JSONObject()
        jsonObject.put("descripcio", content)

        // Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        return requestBody
    }

    fun rawJSONTaskPut(task: Task):RequestBody {
        // Create JSON using JSONObject
        val jsonObject = JSONObject()
        jsonObject.put("idItem", task.id)
        jsonObject.put("descripcio", task.content)
        jsonObject.put("fet", task.done)

        // Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        return requestBody
    }




}

