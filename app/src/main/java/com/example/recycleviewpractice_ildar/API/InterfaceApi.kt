package com.example.recycleviewpractice_ildar.API

import com.example.recycleviewpractice_ildar.models.Lista
import com.example.recycleviewpractice_ildar.models.Task
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Call
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface InterfaceApi {


    @GET()
    suspend fun getLists(@Url url: String):Response<MutableList<Lista>>

    @GET()
    suspend fun getTasks(@Url url: String):Response<MutableList<Task>>


   // @Multipart
    @POST()
    suspend fun postLists(@Url url: String,@Body requestBody: RequestBody): Response<ResponseBody>


    //@Multipart
    @POST()
    suspend fun postItem(@Url url: String,@Body requestBody: RequestBody): Response<ResponseBody>


    @PUT()
    suspend fun updateList(@Url url: String,@Body requestBody: RequestBody): Response<ResponseBody>

    @PUT()
    suspend fun updateTask(@Url url: String,@Body requestBody: RequestBody): Response<ResponseBody>

    @DELETE()
    suspend fun deleteList(@Url url: String,): Response<ResponseBody>

    @DELETE()
    suspend fun deleteTask(@Url url: String,): Response<ResponseBody>



    companion object {
      //  val BASE_URL = "https://profe-api.herokuapp.com"
      val BASE_URL = "https://prueba02122021.herokuapp.com"
        fun createRetrofit(): InterfaceApi {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(InterfaceApi::class.java)
        }
    }



}

/**
CREATE LIST       |@PostMapping("/todolists") public ResponseEntity<?> novaTodoList(@RequestBody TodoList nova) {
CREATE ITEM       |@PostMapping("/todolists/{idList}/todoitems")

GET LISTS         |@GetMapping("/todolists") public ResponseEntity<?> obtenirTotesLesLlistes() {
GET LIST ITEMS    |

UPDATE LIST       |@PutMapping("/todolists")
UPDATE ITEM CONT  |@PutMapping("/todolists/{idList}/todoitems")
UPDATE ITEM DONE  |@PutMapping("/todolists/{idList}/todoitems")

DELETE LIST       |@DeleteMapping("/todolists/{idLlista}")
DELETE ITEM       |@DeleteMapping("/todolists/{idList}/todoitems/{idItem}")

https://johncodeos.com/how-to-make-post-get-put-and-delete-requests-with-retrofit-using-kotlin/







private fun searchJoke(query:String){
CoroutineScope(Dispatchers.IO).launch {
var contador=0
var newJoke=false
val call= createRetrofit().getData("$query/joke/Programming?type=single")
while (!newJoke){
println("EN BUSCA DE CHISTES iteracion NUM "+contador)
contador++

val joke=call.body()
if(call.isSuccessful) {
println("joke sucseful-->" + joke.toString())

if (joke != null) {
if (!ogJokesIds.contains(joke.id)) {
ogJokesIds.add(joke.id)
jokes.add(joke!!)
jokes.forEach { joke -> println("JOKES AHORA num:" + joke.id) }
jokesModel.postValue(jokes)
newJoke = true //prueba, implementalo bien para cuendo sea con un set y concurrente

}else if(contador>20){
newJoke=true
println("NO HAY MAS CHISTES DE PROGRAMMACION")
}
}
}else{
println("ERROR en searchJoke viewModel")

 **/