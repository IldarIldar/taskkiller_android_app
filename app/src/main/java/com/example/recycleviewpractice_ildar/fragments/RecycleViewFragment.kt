package com.example.recycleviewpractice_ildar.fragments
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener
import com.example.recycleviewpractice_ildar.adapters.ListaAdapter
import com.example.recycleviewpractice_ildar.databinding.FragmentRecycleViewBinding
import com.example.recycleviewpractice_ildar.models.Lista
import com.example.recycleviewpractice_ildar.viewModel.ViewModel


class RecycleViewFragment : Fragment(), OnClickListener {

    private lateinit var listAdapter: ListaAdapter;
    private lateinit var binding: FragmentRecycleViewBinding
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    val viewModel: ViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding= FragmentRecycleViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val newLista=binding.addButton
        viewModel.getLists()

        viewModel.listaModel.observe(viewLifecycleOwner, Observer { currentLista->
            println("DETECTO CAMBIOS EN EL VIEWMODEL")
            setUpRecycleView()
            //listAdapter.notifyDataSetChanged()//notifica si  cambio
        })


        newLista.setOnClickListener{

            showDialog()

        }
    }


    fun setUpRecycleView(){
        listAdapter = ListaAdapter(viewModel.listas,this, viewModel)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = listAdapter
        }
    }

    private fun showDialog() {

        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle("Crear nueva lista")
        val input = EditText(context)
        alertDialog.setView(input)

        alertDialog.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            if(input.text.toString().trim().length>0){//si hay algo escrito en el nombre
                viewModel.createNewList(input.text.toString())
            }
        })

        alertDialog.setNegativeButton("close", DialogInterface.OnClickListener { dialog, which -> })
        alertDialog.create().show()
    }

    override fun onClick(lista: Lista) {
        println("--->>>> "+lista)
        val action = RecycleViewFragmentDirections.actionRecycleViewFragmentToRecycleViewTask(lista) //pasamos añ siguiente fragment con un dato student
        findNavController().navigate(action)
    }



}