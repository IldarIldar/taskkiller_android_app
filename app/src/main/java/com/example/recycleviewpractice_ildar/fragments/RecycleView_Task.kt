package com.example.recycleviewpractice_ildar.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener_Task
import com.example.recycleviewpractice_ildar.adapters.TaskAdapter
import com.example.recycleviewpractice_ildar.databinding.FragmentRecycleViewTaskBinding
import com.example.recycleviewpractice_ildar.models.Lista
import com.example.recycleviewpractice_ildar.models.Task
import com.example.recycleviewpractice_ildar.viewModel.ViewModel


class RecycleView_Task : Fragment(), OnClickListener_Task {

    private lateinit var taskAdapter: TaskAdapter;
    private lateinit var binding: FragmentRecycleViewTaskBinding
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    val viewModel: ViewModel by activityViewModels()
    lateinit var thisList:Lista



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding= FragmentRecycleViewTaskBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println("entramos en OnViewCreated de RecycleViewTask")
        var list=arguments?.get("lista") as Lista //recibimos los datos del fragment anterior y casteamos de any a student
        thisList=list
        viewModel.getTasks(list)//limpiamos el mutable live data de taskas y ponemos nuevas taskas


        val newTask=binding.addButton


        viewModel.taskModel.observe(viewLifecycleOwner, Observer {taskasActuales->
            Log.e("DETECTO CAMBIOS", viewModel.taskModel.value.toString())
            setUpRecycleView(list)
           // taskAdapter.notifyDataSetChanged()//notifica si la api cambio
        })


        newTask.setOnClickListener{
            showDialog(list);
        }

    }

    fun setUpRecycleView(list:Lista) {

        taskAdapter = TaskAdapter(viewModel.taskas,this, viewModel, list)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = taskAdapter
        }

    }

        private fun showDialog(list: Lista) {

           val alertDialog = AlertDialog.Builder(context)
           alertDialog.setTitle("Crear nueva tarea")
           val input = EditText(context)
           alertDialog.setView(input)

           alertDialog.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
               viewModel.createNewTask(list, input.text.toString())
           })

           alertDialog.setNegativeButton("close", DialogInterface.OnClickListener { dialog, which -> })
           alertDialog.create().show()

    }



    private fun showUpdateDialog(task: Task) {

        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle("Cambiar tarea")
        val input = EditText(context)
        alertDialog.setView(input)

        alertDialog.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            if(input.text.toString().trim().length>0){//si hay algo escrito en el nombre
                task.content=input.text.toString()
                viewModel.setTaskAtributes(task,thisList)
            }
        })

        alertDialog.setNegativeButton("close", DialogInterface.OnClickListener { dialog, which -> })
        alertDialog.create().show()
    }



   // val clickedTasks = mutableListOf<Task>()
    override fun onClick(task: Task, view: View) {
        showUpdateDialog(task)
    }

    /**
    override fun onLongClick(task: Task, view: View) {
    val item_binding = ItemTaskBinding.bind(view)
    //tiene que sombrear el item para que se vea que ha sido seleccionado, guardarlo en clicked tasks y si pulsas opcion delete eliminarlos del view model
    //cuando sales del activity tiene que poner los items no sombreador o elevados

    }
     */




}