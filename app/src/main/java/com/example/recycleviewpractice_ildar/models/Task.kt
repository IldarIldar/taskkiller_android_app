package com.example.recycleviewpractice_ildar.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Task(@SerializedName("idItem") var id: Int,@SerializedName("descripcio") var content: String,
                @SerializedName("fet") var done:Boolean, @SerializedName("llista")var list:Lista): Parcelable

/**
public class TodoItem {
@Id
@GeneratedValue
private int idItem;
private String descripcio;
private boolean fet;

@ManyToOne
@JoinColumn
private TodoList llista;
}
 **/