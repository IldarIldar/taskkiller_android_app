package com.example.recycleviewpractice_ildar.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Lista(@SerializedName("nomLlista") var name: String,@SerializedName("id") var id:Int): Parcelable

//var tasks: MutableList<Task>
/**
public class TodoItem {
@Id
@GeneratedValue
private int idItem;
private String descripcio;
private boolean fet;

@ManyToOne
@JoinColumn
private TodoList llista;
 **/