package com.example.recycleviewpractice_ildar.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener_Task
import com.example.recycleviewpractice_ildar.R
import com.example.recycleviewpractice_ildar.databinding.ItemTaskBinding
import com.example.recycleviewpractice_ildar.models.Lista
import com.example.recycleviewpractice_ildar.models.Task
import com.example.recycleviewpractice_ildar.viewModel.ViewModel
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.widget.CompoundButton


class TaskAdapter(private val tasks: MutableList<Task>, private val listener: OnClickListener_Task,
                  val viewModel: ViewModel, val list: Lista
)
    : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemTaskBinding.bind(view)

        fun setListener(task: Task) {
            binding.root.setOnClickListener {
                listener.onClick(task,it)
            }
        }

    }

    private lateinit var context: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_task, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasks[position]
        with(holder){
            setListener(task)

            println("estamos en OnBindViewHolder en la tarea "+task.toString())

            binding.contentText.setText(task.content)

            binding.checkbox.isChecked=task.done //set checkbox of view

            //cambia el view model con el checkbox
            binding.checkbox.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                task.done=binding.checkbox.isChecked
                viewModel.setTaskAtributes(task,list)
            })

            //Tachamos texto si hacemos la tarea, destachamos si no
            if(binding.checkbox.isChecked) {
                var spannableString1 = SpannableString(task.content) //texto tachado
                spannableString1.setSpan(StrikethroughSpan(), 0, task.content.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                binding.contentText.text = spannableString1
            }else{
                binding.contentText.text=task.content
            }

            binding.eliminarTaskButton.setOnClickListener {
                viewModel.deleteTask(task,list)
            }
        }
    }

    override fun getItemCount(): Int {
        return tasks.size
    }


}