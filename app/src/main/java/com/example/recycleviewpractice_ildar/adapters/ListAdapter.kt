package com.example.recycleviewpractice_ildar.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener
import com.example.recycleviewpractice_ildar.R
import com.example.recycleviewpractice_ildar.databinding.ItemListBinding
import com.example.recycleviewpractice_ildar.models.Lista
import com.example.recycleviewpractice_ildar.viewModel.ViewModel

class ListaAdapter (private val listas: List<Lista>, private val listener: OnClickListener,val viewModel: ViewModel)
:RecyclerView.Adapter<ListaAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemListBinding.bind(view)

        fun setListener(lista: Lista) {
            binding.root.setOnClickListener {
                listener.onClick(lista)
            }
        }

    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lista = listas[position]
        with(holder){
            setListener(lista)
            binding.name.text = lista.name


            binding.updateButton.setOnClickListener{
                showUpdateDialog(lista)
            }

            binding.eliminarListButton.setOnClickListener {
               // listener.onDelete(lista)
                viewModel.deleteList(lista)
            }



        }
    }

    override fun getItemCount(): Int {
        return listas.size
    }


    private fun showUpdateDialog(lista: Lista) {

        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle("Cambiar nombre lista")
        val input = EditText(context)
        alertDialog.setView(input)

        alertDialog.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            if(input.text.toString().trim().length>0){//si hay algo escrito en el nombre
                lista.name=input.text.toString()
                viewModel.setListAtributes(lista)
            }
        })

        alertDialog.setNegativeButton("close", DialogInterface.OnClickListener { dialog, which -> })
        alertDialog.create().show()
    }
}